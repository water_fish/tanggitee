# Python代码中执行另外一个.py文件

关于Python 脚本如何执行另一个脚本，可以使用os.system()来实现

简单粗暴的方式

```
import os
os.system("python filename.py")12
```

如一个main.py

```
import os
os.system("python /home/xx/projects/testcode/subcode.py")12
```

而subcode.py

```
def sum():
    x = 1
    y = 2
    print(x+y)
sum()12345
```

一般：

```
import os
str=('python B.py')   //python命令 + B.py 
p=os.system(str)
print(p)    //打印执行结果 0表示 success ， 1表示 fail
```



关于Python 脚本如何执行另一个脚本，可以使用os.system()来实现

------

os.system()的参数： 执行的命令 +执行的内容
举例说明：

```
（1）显示当前文件夹下的全部目录和文件夹
os.system('dir')   //dir 显示磁盘目录命令

（2）删除指定文件夹下的文件
os.system('del e:\\test\\test.txt')  //del 删除指定文件 + 要删除文件名


（3）删除一个空文件夹
os.system('rd e:\\test')  //rd(RMDIR):在DOS操作系统中用于删除一个目录 + 要删除文件夹


（4）关闭进程
os.system('taskkill  /F /IM chrome.exe') //taskkill是用来终止进程的 + 进程名1234567891011121314151617
```

本实验中用A.py 执行B.py，其中B.py执行时需要添加参数（cmd下执行命令：`>Python B.py IC.txt`）
A.py 实验代码如下：

```
import os
str=('python B.py IC.txt')   //python命令 + B.py + 参数：IC.txt'
p=os.system(str)
print(p)   //打印执行结果 0表示 success ， 1表示 fail1234
```

如果B.py执行时不需要添加参数（cmd下执行命令：`>Python B.py`）

```
import os
str=('python B.py')   //python命令 + B.py 
p=os.system(str)
print(p)    //打印执行结果 0表示 success ， 1表示 fail
```