# 训练自定义数据(Train Custom Data)

英文文档来源:[Train Custom Data · ultralytics/yolov5 Wiki (github.com)](https://github.com/ultralytics/yolov5/wiki/Train-Custom-Data)

本指南解释如何使用YOLOv5训练您自己的自定义数据集🚀。

📚 This guide explains how to train your own **custom dataset** with YOLOv5 🚀. 

## 写在前面(Before You Start)

克隆此repo,下载教程数据集,并安装requirements.txt(pip install -r requirements.txt),需要python>=3.6和PyTorch>=1.7.

Clone this repo, download tutorial dataset, and install [requirements.txt](https://github.com/ultralytics/yolov5/blob/master/requirements.txt), including **Python>=3.6.0** and **PyTorch>=1.7**.

```
$ git clone https://github.com/ultralytics/yolov5  # clone repo
$ cd yolov5
$ pip install -r requirements.txt  # install
```

## 训练自定义数据(Train On Custom Data)

### 1. 创建dataset.yaml(Create dataset.yaml)

COCO128是一个小型教程数据集，由COCO train2017中的前128张图像组成。这些相同的128幅图像用于训练和验证，以验证我们的训练管道能够过拟合。data/coco128.yaml,如下所示，是数据集配置文件，它定义了1)数据集根目录和到 train/val/test 图像目录的相对路径（或带有图像路径的 *.txt 文件的路径），2)类的数量和3)类名列表:

[COCO128](https://www.kaggle.com/ultralytics/coco128) is a small tutorial dataset composed of the first 128 images in [COCO](http://cocodataset.org/#home) train2017. These same 128 images are used for both training and validation to verify our training pipeline is capable of overfitting. [data/coco128.yaml](https://github.com/ultralytics/yolov5/blob/master/data/coco128.yaml), shown below, is the dataset configuration file that defines 1) the dataset root directory and relative paths to train/val/test image directories (or paths to *.txt files with image paths), 2) the number of classes and 3) a list of class names:

```
# Train/val/test sets as 1) dir: path/to/imgs, 2) file: path/to/imgs.txt, or 3) list: [path/to/imgs1, path/to/imgs2, ..]
path: ../datasets/coco128  # dataset root dir
train: images/train2017  # train images (relative to 'path') 128 images
val: images/train2017  # val images (relative to 'path') 128 images
test:  # test images (optional)

# Classes
nc: 80  # number of classes
names: [ 'person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
         'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow',
         'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee',
         'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard',
         'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
         'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch',
         'potted plant', 'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone',
         'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors', 'teddy bear',
         'hair drier', 'toothbrush' ]  # class names
```

### 2. 创建标签(Create Labels)

使用 CVAT 或 makesense.ai 等工具标记图像后，将标签导出为 YOLO 格式，每个图像一个文件（如果图像中没有对象，则不需要文件）。 文件规格为： `*.txt``*.txt``*.txt`

每个对象一行

每行都是format.`class x_center y_center width height`

框坐标必须采用标准化 xywh 格式（从 0 - 1）。 如果您的框以像素为单位，除以图像宽度和图像高度。`x_center` `width` `y_center` `height`

类号是零索引的（从 0 开始）。

After using a tool like [CVAT](https://github.com/opencv/cvat) or [makesense.ai](https://www.makesense.ai/) to label your images, export your labels to **YOLO format**, with one file per image (if no objects in image, no file is required). The file specifications are:`*.txt``*.txt``*.txt`

- One row per object
- Each row is format.`class x_center y_center width height`
- Box coordinates must be in **normalized xywh** format (from 0 - 1). If your boxes are in pixels, divide and by image width, and and by image height.`x_center` `width` `y_center` `height`
- Class numbers are zero-indexed (start from 0).

![Image Labels](E:%5CUsers%5CHP%5CDesktop%5Cmd%E6%96%87%E4%BB%B6%5Ctupian%5C91506361-c7965000-e886-11ea-8291-c72b98c25eec.jpg)

上图对应的标签文件包含2个人（class）和一条领带（class）：`0`  ` 27`

The label file corresponding to the above image contains 2 persons (class ) and a tie (class ):`0` `27`

![img](E:%5CUsers%5CHP%5CDesktop%5Cmd%E6%96%87%E4%BB%B6%5Ctupian%5C112467037-d2568c00-8d66-11eb-8796-55402ac0d62f.png)

### 3.组织目录( Organize Directories)

根据以下示例组织您的训练和验证图像和标签。 在这个例子中，我们假设 是在目录旁边。 YOLOv5 通过将每个图像路径中的最后一个实例替换为 . 例如：`/coco128` `/yolov5` `/images/` `/labels/` 

Organize your train and val images and labels according to the example below. In this example we assume is **next to** the directory. **YOLOv5 locates labels automatically for each image** by replacing the last instance of in each image path with . For example: `/coco128` `/yolov5` `/images/` `/labels/`

```
dataset/images/im0.jpg  # image
dataset/labels/im0.txt 
# label
```

![img](E:%5CUsers%5CHP%5CDesktop%5Cmd%E6%96%87%E4%BB%B6%5Ctupian%5C112467887-e18a0980-8d67-11eb-93af-6505620ff8aa.png)

### 4. Select a Model

Select a pretrained model to start training from. Here we select [YOLOv5s](https://github.com/ultralytics/yolov5/blob/master/models/yolov5s.yaml), the smallest and fastest model available. See our README [table](https://github.com/ultralytics/yolov5#pretrained-checkpoints) for a full comparison of all models.

![YOLOv5 Models](https://github.com/ultralytics/yolov5/releases/download/v1.0/model_comparison.png)

### 5. Train

Train a YOLOv5s model on COCO128 by specifying dataset, batch-size, image size and either pretrained (recommended), or randomly initialized (not recommended). Pretrained weights are auto-downloaded from the [latest YOLOv5 release](https://github.com/ultralytics/yolov5/releases).`--weights yolov5s.pt``--weights '' --cfg yolov5s.yaml`

```
# Train YOLOv5s on COCO128 for 5 epochs
$ python train.py --img 640 --batch 16 --epochs 5 --data coco128.yaml --weights yolov5s.pt
```

All training results are saved to with incrementing run directories, i.e. , etc. For more details see the Training section of our Google Colab Notebook. `runs/train/``runs/train/exp2``runs/train/exp3`[![Open In Colab](https://camo.githubusercontent.com/84f0493939e0c4de4e6dbe113251b4bfb5353e57134ffd9fcab6b8714514d4d1/68747470733a2f2f636f6c61622e72657365617263682e676f6f676c652e636f6d2f6173736574732f636f6c61622d62616467652e737667)](https://colab.research.google.com/github/ultralytics/yolov5/blob/master/tutorial.ipynb) [![Open In Kaggle](E:%5CUsers%5CHP%5CDesktop%5Cmd%E6%96%87%E4%BB%B6%5Ctupian%5C68747470733a2f2f6b6167676c652e636f6d2f7374617469632f696d616765732f6f70656e2d696e2d6b6167676c652e737667.svg)](https://www.kaggle.com/ultralytics/yolov5)

## Visualize

### Weights & Biases Logging (🚀 NEW)

[Weights & Biases](https://wandb.ai/site?utm_campaign=repo_yolo_traintutorial) (W&B) is now integrated with YOLOv5 for real-time visualization and cloud logging of training runs. This allows for better run comparison and introspection, as well improved visibility and collaboration among team members. To enable W&B logging install , and then train normally (you will be guided setup on first use).`wandb`

```
$ pip install wandb
```

During training you will see live updates at [https://wandb.ai](https://wandb.ai/site?utm_campaign=repo_yolo_traintutorial), and you can create [Detailed Reports](https://wandb.ai/glenn-jocher/yolov5_tutorial/reports/YOLOv5-COCO128-Tutorial-Results--VmlldzozMDI5OTY) of your results using the W&B Reports tool.

![img](E:%5CUsers%5CHP%5CDesktop%5Cmd%E6%96%87%E4%BB%B6%5Ctupian%5C112469341-a8eb2f80-8d69-11eb-959a-dd85d3997bcf.jpg)

### Local Logging

All results are logged by default to , with a new experiment directory created for each new training as , , etc. View train and val jpgs to see mosaics, labels, predictions and augmentation effects. Note a **Mosaic Dataloader** is used for training (shown below), a new concept developed by Ultralytics and first featured in [YOLOv4](https://arxiv.org/abs/2004.10934).`runs/train``runs/train/exp2``runs/train/exp3`

`train_batch0.jpg` shows train batch 0 mosaics and labels:

> ![img](E:%5CUsers%5CHP%5CDesktop%5Cmd%E6%96%87%E4%BB%B6%5Ctupian%5C124931219-48bf8700-e002-11eb-84f0-e05d95b118dd.jpg)

`val_batch0_labels.jpg` shows val batch 0 labels:

> ![img](E:%5CUsers%5CHP%5CDesktop%5Cmd%E6%96%87%E4%BB%B6%5Ctupian%5C124931217-4826f080-e002-11eb-87b9-ae0925a8c94b.jpg)

`val_batch0_pred.jpg` shows val batch 0 *predictions*:

> ![img](E:%5CUsers%5CHP%5CDesktop%5Cmd%E6%96%87%E4%BB%B6%5Ctupian%5C124931209-46f5c380-e002-11eb-9bd5-7a3de2be9851.jpg)

Training results are automatically logged to [Tensorboard](https://www.tensorflow.org/tensorboard) and , which is plotted as (below) after training completes. You can also plot any file manually:`runs/train/exp/results.txt``results.png``results.txt`

```
from utils.plots import plot_results 
plot_results(save_dir='runs/train/exp')  # plot all results*.txt files in 'runs/train/exp'
```

![COCO128 Training Results](https://github.com/ultralytics/yolov5/releases/download/v1.0/results_COCO128.png)

## Environments

YOLOv5 may be run in any of the following up-to-date verified environments (with all dependencies including [CUDA](https://developer.nvidia.com/cuda)/[CUDNN](https://developer.nvidia.com/cudnn), [Python](https://www.python.org/) and [PyTorch](https://pytorch.org/) preinstalled):

- **Google Colab and Kaggle** notebooks with free GPU: [![Open In Colab](E:%5CUsers%5CHP%5CDesktop%5Cmd%E6%96%87%E4%BB%B6%5Ctupian%5C68747470733a2f2f636f6c61622e72657365617263682e676f6f676c652e636f6d2f6173736574732f636f6c61622d62616467652e737667.svg)](https://colab.research.google.com/github/ultralytics/yolov5/blob/master/tutorial.ipynb) [![Open In Kaggle](https://camo.githubusercontent.com/a08ca511178e691ace596a95d334f73cf4ce06e83a5c4a5169b8bb68cac27bef/68747470733a2f2f6b6167676c652e636f6d2f7374617469632f696d616765732f6f70656e2d696e2d6b6167676c652e737667)](https://www.kaggle.com/ultralytics/yolov5)
- **Google Cloud** Deep Learning VM. See [GCP Quickstart Guide](https://github.com/ultralytics/yolov5/wiki/GCP-Quickstart)
- **Amazon** Deep Learning AMI. See [AWS Quickstart Guide](https://github.com/ultralytics/yolov5/wiki/AWS-Quickstart)
- **Docker Image**. See [Docker Quickstart Guide](https://github.com/ultralytics/yolov5/wiki/Docker-Quickstart) [![Docker Pulls](E:%5CUsers%5CHP%5CDesktop%5Cmd%E6%96%87%E4%BB%B6%5Ctupian%5C68747470733a2f2f696d672e736869656c64732e696f2f646f636b65722f70756c6c732f756c7472616c79746963732f796f6c6f76353f6c6f676f3d646f636b6572.svg)](https://hub.docker.com/r/ultralytics/yolov5)

## Status

![CI CPU testing](https://github.com/ultralytics/yolov5/workflows/CI%20CPU%20testing/badge.svg)

If this badge is green, all [YOLOv5 GitHub Actions](https://github.com/ultralytics/yolov5/actions) Continuous Integration (CI) tests are currently passing. CI tests verify correct operation of YOLOv5 training ([train.py](https://github.com/ultralytics/yolov5/blob/master/train.py)), validation ([val.py](https://github.com/ultralytics/yolov5/blob/master/val.py)), inference ([detect.py](https://github.com/ultralytics/yolov5/blob/master/detect.py)) and export ([export.py](https://github.com/ultralytics/yolov5/blob/master/export.py)) on MacOS, Windows, and Ubuntu every 24 hours and on every commit.

Ultralytics 2021
[https://ultralytics.com](https://ultralytics.com/)

[About Us](https://ultralytics.com/about-us)
[Join Our Team](https://ultralytics.com/work)
[Contact Us](https://ultralytics.com/contact)

[Terms of Use](https://ultralytics.com/terms-of-use)
[Privacy Policy](https://ultralytics.com/privacy)