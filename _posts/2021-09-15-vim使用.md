# vim使用
`ll` `ls -l`的别名，在一些不支持`ll`的地方可直接使用`ls -l`来显示`ll`

`mkdir 文件夹名`创建文件夹

`rm -rf 文件名`删除文件

`cp 原文件路径 目的路径 `复制单个文件

`cp 原文件路径 -r 目的路径 `复制文件夹

`mv 原文件或文件夹名 重命名后的名字`重命名文件或文件夹

查看文件:`cat 文件名`

将查找后的内容另存为别的文件`grep 查找内容 文件名>新文件名  `

`cat 文件名 | grep 查找内容 文件名>新文件名`



添加权限

`chmod +x 文件名`

创建test.sh:

```
date
```

添加x权限

```
chmod +x test.sh
```

运行sh文件

```
./test.sh
```

```
[test@iZuf64fh3qqfih9qs3bh1oZ tang]$ vi test.sh
[test@iZuf64fh3qqfih9qs3bh1oZ tang]$ ll
total 36
drwxrwxr-x 9 test test  4096 Sep 15 17:15 1
-rw-rw-r-- 1 test test   878 Sep 15 17:32 SentimentModel_def
-rw-rw-r-- 1 test test 14794 Sep 15 17:10 SentimentModel.py
-rw-rw-r-- 1 test test     5 Sep 15 20:22 test.sh
-rw-rw-r-- 1 test test    76 Sep 15 19:16 test.txt
drwxrwxr-x 2 test test  4096 Sep 15 19:21 xieqiang
[test@iZuf64fh3qqfih9qs3bh1oZ tang]$ chmod +x test.sh
[test@iZuf64fh3qqfih9qs3bh1oZ tang]$ ll
total 36
drwxrwxr-x 9 test test  4096 Sep 15 17:15 1
-rw-rw-r-- 1 test test   878 Sep 15 17:32 SentimentModel_def
-rw-rw-r-- 1 test test 14794 Sep 15 17:10 SentimentModel.py
-rwxrwxr-x 1 test test     5 Sep 15 20:22 test.sh
-rw-rw-r-- 1 test test    76 Sep 15 19:16 test.txt
drwxrwxr-x 2 test test  4096 Sep 15 19:21 xieqiang
[test@iZuf64fh3qqfih9qs3bh1oZ tang]$ ./test.sh 
Wed Sep 15 20:23:15 CST 2021
```



创建空文件`touch 文件名`



显示Linux下文本文件的换行符
`cat -A filename`

复制粘贴
`yy`	复制整行
`p`	粘贴

删除行
`dd`	删除光标所在行
`ndd`	删除光标所在的向下n行
`dG`	删除光标所在到最后一行的数据

删除选中的文字
如果在命令状态，使用`v`或`Ctrl+v`选中一段文字，然后按`x`

复制选中文字
如果在命令状态，使用`v`或`Ctrl+v`选中一段文字，然后按`y`复制，按`p(或P)`粘贴 

显示行号
`:set nu`

取消行号
`:set nonu`

多文件编辑
`:n`	编辑下一个文件
`:N`	编辑上一个文件
`:files`	列出当前vim打开的所有文件

DOS与Linux的换行符
dos2 unix [-kn] file [newfile]	转换为Unix
unix2 dos [-kn] file [newfile]	转换为dos

通配符与特殊符号
`*`代表0个或多个字符（或数字）
`?`代表“一定有”一个字母
`#`注释，常用在脚本中，视为说明
`\`转义字符，将“特殊字符或通配符”还原成一般字符

`|` 分隔两个管道的命令

`;`连续命令的分隔（注意，与管道命令不同）

`&`将命令变成后台工作

`!`逻辑意义上的“非”（not）的意思

`/`路径分隔的符号

`>` `>>`输出导向，分别是“替换”与“追加”

`'`	不具备变量置换功能

`”`	具备变量置换功能

```
``两个“`”中间为可以先执行的命令
```

`()`	中间为子shell的起始和结束

`[]`中间为字符的组合

`{}`	中间为命令区块的组合



管道命令（pipe）
1、cut
`echo $PATH|cut -d ':' -f 3`

2、去除shell的\r
`cat my_shell.sh|tr -d '\r' > my_shell.sh`

//------------------------正则表达式----------------------------------------------------------

1、`grep [-acinv]` '搜索字符串' filenames

参数说明：
`-a `:在二进制文件中，以文本文件方式搜索数据
`-c `:计算找到'搜索'字符串的次数
`-i `:忽略大小写的不同
`-n` :输出行号
`-v `:反向选择，即显示没有'搜索字符串'内容的那一行

2、利用[]来搜索集合字符串

`grep -n 't[ae]st' filenames`	[]中不论有几个字符，都只代表一个字符，即至搜索'tast'和'test'两个字符串
`grep -n '[^g]oo' filenames`	取出oo前面不含g的字符串
`grep -n '[^a-z]oo' filenames`	取出oo前面不含小写字母的字符串（如果要求是数字和英文，则为[a-zA-Z0-9]）

3、行首与行尾字符 ^ $

```
grep -n '^the' filenames	取出行首包含the的字符串（注意，^在[]之内和之外是不同的，在[]表示反向选择，在[]之外则代表定位在行首）
grep -n '\.$' filenames		取出行尾有'.'的行
```

4、任意一个字符 . 与重复字符 *

`grep -n 'g..d' filenames`	取出包含g、d字符的行，且g、d字符之间一定要存在两个字符
`grep -n 'oo*' filenames`		取出包含1个及以上o的行（*代表0个或多个任意字符）

5、限定重复字符范围 {}

`grep -n 'o\{2,5\}' filename`	取出包含2~5个o的行（注意:{}在shell中有特殊含义，使用是需要转义符\）

6、sed工具简介

删除
`cat -n /etc/passwd | sed '2,2d'	`	删除第2到第12行
`cat -n /etc/passwd | sed '12,$d'`	删除第12行到最后一行增加
`cat -n /etc/passwd | sed '2a hello wanlx'`	在第2行后新增hello wlx
`cat -n /etc/passwd | sed '5i hello wanlx'	`在第5行后新增hello wlx替换
`cat -n /etc/passwd | sed '2,5c hello wanlx'`	将2-5行替换为hello wlx打印
`cat -n /etc/passwd | sed -n '5,7p'	`	显示5-7行

7、`awk`工具简介（好东西）

按空格和Tab切割字符串

//--------------------学习shell脚本-------------------------------------------------------------

1、使用判断符号 []
	1.1 在每个组件中间，都要用空格分隔，例如：`[ "$HOME" == "$MAIL" ]`
	1.2 中括号内的变量，最好用双引号来设置。
	1.3 中括号内的常量，最好用单引号或双引号来设置。

2、shell脚本的追踪与调试
`sh [-nvx] filename.sh`
`-n`	不要执行脚本，仅查询语法问题
`-v`	在执行脚本前，先将脚本的内容输出到屏幕上
`-x`	将使用的脚本内容显示到屏幕上（追踪脚本的执行）

//----------------循环执行的例行性命令--------------------------------------------------

`crontab [-u username] [-l|-e|-r]`
参数
`-u`	只有`root`才能执行这个任务，即帮其他用户建立/删除`crontab`
`-e`	编辑`crontab`的工作内容
`-l`	查看`crontab`的工作内容
`-r`	删除`crontab`的工作内容(全部内容)

`30 10 * * * /bin/sh /root/shell_script/mongodb/mongo_log_mgr.sh`

编辑任务
`crontab -e`
`*/1 * * * * cd /home/wanlx/myworkspace;date >> date.wlx`
分	时	日	月	周

编辑完成后重启`crond`服务(`redhat6.4`可以不重启)
`/etc/init.d/crond restart`

编译系统发送邮件的地址
`vi /etc/crontab` 然后修改`MAILTO`

//---------------管理后台作业-------------------------------------------

`kill -signal %jobnumber`
参数
`-l`	这个是L的小写，列出当前kill能使用的信号(signal)
`signal`	表示给后面的作业什么指示（不知道干啥的）。用man 7 signal可知
`-1`	表示重新读取一次参数的设置文件（类似reload）
`-2`	表示与由键盘输入`[ctrl]-c`同样的动作
`-9`	立刻强制删除一个作业
`-15`	以正常的程序方式终止一项作业

//----------------------------------------------------------------------